package com.example.revoluttest;

import android.os.Bundle;

import com.example.revoluttest.adapters.SectionsPagerAdapter;
import com.example.revoluttest.base.BaseActivity;
import com.example.revoluttest.base.RevolutApplication;
import com.example.revoluttest.databinding.ActivityMainBinding;
import com.example.revoluttest.ui.main.fragments.AddRatesFragment;
import com.example.revoluttest.ui.main.fragments.ConverterFragment;
import com.google.android.material.tabs.TabLayout;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import javax.inject.Inject;

public class MainActivity extends BaseActivity {

    @Inject
    public AddRatesFragment addRatesFragment;

    @Inject
    public ConverterFragment converterFragment;

    @Override
    protected int layoutRes() {
        return R.layout.activity_main;
    }
    TabLayout.OnTabSelectedListener onTabSelectedListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, this.getSupportFragmentManager(), initFragments());
        ActivityMainBinding activityMainBinding = DataBindingUtil.getBinding(dataBinding.getRoot());
        if (activityMainBinding == null) return;
        setTabListener();
        activityMainBinding.viewPager.setAdapter(sectionsPagerAdapter);
        activityMainBinding.tabs.setupWithViewPager(activityMainBinding.viewPager);
        activityMainBinding.tabs.addOnTabSelectedListener(onTabSelectedListener);
    }

    private Fragment[] initFragments() {
        Fragment[] fragments = new Fragment[0x02];
        fragments[0] = (addRatesFragment);
        fragments[1] = (converterFragment);
        return fragments;
    }

    public void setTabListener() {
        onTabSelectedListener = new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabUnselected(TabLayout.Tab tab) { }
            @Override
            public void onTabReselected(TabLayout.Tab tab){}
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0x00:
                        converterFragment.stopPolling();
                        addRatesFragment.isStoppedPolling();
                        converterFragment.isStopped = false;

                        break;
                    case 0x01:
                        addRatesFragment.stopPolling();
                        converterFragment.revokeAdapter();
                        break;
                    default:
                        break;
                }
            }
        };
    }
}