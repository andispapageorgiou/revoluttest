package com.example.revoluttest.services;

import android.os.Handler;
import android.os.HandlerThread;

import com.example.revoluttest.models.CurrencyResponse;
import com.example.revoluttest.models.RatesAdapterItem;
import com.jakewharton.rxrelay2.BehaviorRelay;
import com.mynameismidori.currencypicker.ExtendedCurrency;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class RatesRequester {
    private final Service service;

    @Inject
    RatesRequester(Service service) {
        this.service = service;
        refreshRatesHandlerThread = new HandlerThread("RefreshRatesHandlerThread");
        refreshRatesHandlerThread.start();
    }

    /**
     * EUR API Request
     * @param currency
     * @return
     */
    private Single<Object> subscribeToRates(String currency) {
        return service.getCurrencyResponse(currency).map(CurrencyResponse::rates);
    }

    private int counterId;
    private HandlerThread refreshRatesHandlerThread;
    private Runnable refreshRatesRunnable;
    private List<Disposable> subscriptions = new ArrayList<>();
    public List<ExtendedCurrency> extendedCurrencyList;
    private Handler refreshRatesHandler;

    /**
     * Method that starts polling / invoking Rates per 1000 milliSeconds
     *
     * @param ratesLoaded
     */
    @SuppressWarnings("unchecked")
    public void getRatesInitializeTimer(BehaviorRelay ratesLoaded) {
        if (refreshRatesHandler != null) return;
        refreshRatesHandler = new Handler(refreshRatesHandlerThread.getLooper());
        refreshRatesRunnable = () -> {
            subscriptions.add(subscribeToRates("EUR")
                    .doOnError(Throwable::printStackTrace)
                    .observeOn(Schedulers.io())
                    .subscribe((ratesLinkedHashMapTree, throwable) -> {
                        List<RatesAdapterItem> ratesAdapterItemList = new ArrayList<>();
                        counterId = 0;

                        if (ratesLinkedHashMapTree == null) return;
                        ((AbstractMap<String, Double>) ratesLinkedHashMapTree).forEach((key, value)
                                -> ratesAdapterItemList.add(new RatesAdapterItem(counterId++, key, value)));
                        ratesLoaded.accept(ratesAdapterItemList);
                    }));
            refreshRatesHandler.postDelayed(refreshRatesRunnable, 1000);
        };
        startPolling();
    }

    public void startPolling() {
        if (refreshRatesHandler == null) return;
        refreshRatesHandler.post(refreshRatesRunnable);
    }

    public void stopPolling() {
        if (refreshRatesHandler == null) return;
        refreshRatesHandler.removeCallbacks(refreshRatesRunnable);
    }

    public void onDetach() {
        for (Disposable disposable : subscriptions) {
            if (disposable != null && !disposable.isDisposed())
                disposable.dispose();
        }
    }

    public List<ExtendedCurrency> getExtendedCurrencies() {
        if (extendedCurrencyList == null)
            extendedCurrencyList = ExtendedCurrency.getAllCurrencies();
        return extendedCurrencyList;
    }
}
