package com.example.revoluttest.services;

import com.example.revoluttest.models.CurrencyResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Service {
    @GET("latest")
    Single<CurrencyResponse> getCurrencyResponse(@Query(value = "base", encoded = true) String currency);
}
