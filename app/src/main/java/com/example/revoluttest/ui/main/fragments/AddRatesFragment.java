package com.example.revoluttest.ui.main.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;

import com.example.revoluttest.R;
import com.example.revoluttest.adapters.RatesRecyclerAdapter;
import com.example.revoluttest.base.BaseFragment;
import com.example.revoluttest.databinding.FragmentAddRatesBinding;
import com.example.revoluttest.interfaces.IFragmentImplementation;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class AddRatesFragment extends BaseFragment implements IFragmentImplementation {

    @Inject
    public AddRatesFragment() {
    }

    private FragmentAddRatesBinding fragmentAddRatesBinding;

    @Override
    public int layoutRes() {
        return R.layout.fragment_add_rates;
    }

    @Override
    public ViewDataBinding dataBinding() {
        return fragmentAddRatesBinding;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        fragmentAddRatesBinding = (FragmentAddRatesBinding) dataBinding;
        revokeAdapter();
    }

    @Override
    public void revokeAdapter() {
        ratesRecyclerAdapterRates = new RatesRecyclerAdapter(this, RatesRecyclerAdapter.RATES_VIEW, new ArrayList<>());
        fragmentAddRatesBinding.ratesRecyclerView.setAdapter(ratesRecyclerAdapterRates);
        ratesRecyclerAdapterRates.subscribeToRatesUpdate();
        startPolling();
    }

    @Override
    public boolean stopPolling() {
        ratesVM.stopPooling();
        return false;
    }

    @Override
    public boolean startPolling() {
        ratesVM.invokeRates();
        return true;
    }

    @Override
    public boolean isStoppedPolling() {
        return false;
    }

    @Override
    public void attachInput(CharSequence charSequence) {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        for (Disposable disposable : ratesVM.subscriptions) {
            if (disposable != null && !disposable.isDisposed())
                disposable.dispose();
        }
        ratesVM.onDetach();
    }
}