package com.example.revoluttest.ui.main.fragments;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.DiffUtil;

import com.example.revoluttest.R;
import com.example.revoluttest.adapters.DiffCallBackRates;
import com.example.revoluttest.adapters.RatesRecyclerAdapter;
import com.example.revoluttest.base.BaseFragment;
import com.example.revoluttest.databinding.FragmentConverterBinding;
import com.example.revoluttest.interfaces.IFragmentImplementation;
import com.example.revoluttest.models.RatesAdapterItem;
import com.example.revoluttest.utils.EditTextListeners;
import com.example.revoluttest.widget.RecyclerItemClickListener;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class ConverterFragment extends BaseFragment implements IFragmentImplementation {

    private FragmentConverterBinding fragmentConverterBinding;
    private EditTextListeners editTextListeners = new EditTextListeners(this);
    private RatesAdapterItem selectedItem;
    public boolean isStopped;

    @Inject
    ConverterFragment() {
    }

    @Override
    public int layoutRes() {
        return R.layout.fragment_converter;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        fragmentConverterBinding = (FragmentConverterBinding) dataBinding;
    }

    @Override
    public void revokeAdapter() {
        startPolling();

        ratesRecyclerAdapter = new RatesRecyclerAdapter(this, RatesRecyclerAdapter.CONVERTER_VIEW, new ArrayList<>());
        fragmentConverterBinding.converterRecyclerView.setAdapter(ratesRecyclerAdapter);
        fragmentConverterBinding.converterRecyclerView.getRecycledViewPool().setMaxRecycledViews(0, 0);
        ratesRecyclerAdapter.subscribeToRatesUpdate();

        fragmentConverterBinding.converterRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), (view, position) -> {
                    try {
                        stopPolling();
                        if (ratesRecyclerAdapter != null
                                && this.ratesVM.ratesLiveData.getValue() != null
                                && ratesVM.ratesLiveData.getValue().size() > 0
                                && ratesVM.ratesLiveData.getValue().size() > position) {

                            selectedItem = this.ratesVM.ratesLiveData.getValue().get(position);
                            ratesRecyclerAdapter.notifyItemMoved(selectedItem.getId(), 0);
                            fragmentConverterBinding.converterRecyclerView.scrollToPosition(0);

                            if (view instanceof ViewGroup) {
                                for (int i = 0; i <= ((ViewGroup) view).getChildCount(); i++) {
                                    View child = ((ViewGroup) view).getChildAt(i);
                                    if (child instanceof TextInputEditText) {
                                        child.requestFocus();
                                        ((TextInputEditText) child).setText("");
                                        ((TextInputEditText) child).addTextChangedListener(editTextListeners.getTextWatcher());
                                        if (getActivity() != null) {
                                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                            if (imm != null)
                                                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

                                            stopPolling();                                       }
                                    }
                                }
                            }
                        }
                    } catch (IndexOutOfBoundsException e) {
                        e.printStackTrace();
                    }
                }));
    }

    @Override
    public synchronized void attachInput(CharSequence charSequence) {
        try {
            if (TextUtils.isEmpty(charSequence) && this.ratesVM.ratesLiveData.getValue() != null) {
                this.ratesRecyclerAdapter.getResults().accept(new Pair<>(diffResult, this.ratesVM.ratesLiveData.getValue()));
            }
            if (charSequence == null || selectedItem == null || fragmentConverterBinding.converterRecyclerView.isComputingLayout()) return;
            if (charSequence.toString().equals("0")) return;
            this.ratesRecyclerAdapter.currencyConverterFilter(selectedItem,
                    charSequence.toString().equals("") ? 1 : Double.parseDouble(charSequence.toString()));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean stopPolling() {
        ratesVM.stopPooling();
        isStopped = true;
        return false;
    }

    @Override
    public boolean startPolling() {
        ratesVM.startPolling();
        isStopped = false;
        return true;
    }

    @Override
    public boolean isStoppedPolling() {
        return isStopped;
    }

    @Override
    public ViewDataBinding dataBinding() {
        return fragmentConverterBinding;
    }
}
