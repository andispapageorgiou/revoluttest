package com.example.revoluttest.ui.main.viewmodel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.example.revoluttest.services.RatesRequester;
import com.example.revoluttest.models.RatesAdapterItem;
import com.jakewharton.rxrelay2.BehaviorRelay;
import com.mynameismidori.currencypicker.ExtendedCurrency;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

public class RatesVM extends AndroidViewModel {

    private RatesRequester ratesRequester;
    public List<Disposable> subscriptions = new ArrayList<>();
    private BehaviorRelay<List<RatesAdapterItem>> ratesLoaded = BehaviorRelay.create();
    public MutableLiveData<List<RatesAdapterItem>> ratesLiveData = new MutableLiveData<>();
    private Context context;

    private BehaviorRelay<List<RatesAdapterItem>> ratesResultResponseHashMap() {
        return ratesLoaded;
    }

    public RatesVM(@NonNull Application application) {
        super(application);
        subscriptions.add(ratesResultResponseHashMap()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(ratesAdapterItems -> {
                    for (ExtendedCurrency extendedCurrency : ratesRequester.getExtendedCurrencies()) {
                        for (RatesAdapterItem ratesAdapterItem : ratesAdapterItems) {
                            if (extendedCurrency.getCode().equals(ratesAdapterItem.getCode())) {
                                try {
                                    ratesAdapterItem.flag = ResourcesCompat.getDrawable(context.getResources(), extendedCurrency.getFlag(), null);
                                    ratesAdapterItem.name = extendedCurrency.getName();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                    ratesLiveData.setValue(ratesAdapterItems);
                }));
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setPageRequester(RatesRequester ratesRequester) {
        this.ratesRequester = ratesRequester;
    }

    public void invokeRates() {
        if (ratesRequester == null) return;
        ratesRequester.getRatesInitializeTimer(ratesLoaded);
    }

    public void startPolling() {
        ratesRequester.getRatesInitializeTimer(ratesLoaded);
        ratesRequester.startPolling();
    }

    public void stopPooling() {
        ratesRequester.stopPolling();
    }

    public synchronized void onDetach() {
        for (Disposable disposable : subscriptions) {
            if (disposable != null && !disposable.isDisposed())
                disposable.dispose();
        }
        ratesRequester.stopPolling();
        this.ratesRequester.onDetach();
    }

}