package com.example.revoluttest.utils;

import android.annotation.SuppressLint;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.example.revoluttest.interfaces.IFragmentImplementation;
import com.example.revoluttest.widget.AppTextInputEditText;

public class EditTextListeners {

    private TextWatcher textWatcher;
    private View.OnTouchListener onTouchListener;
    private IFragmentImplementation fragment;
    private final static String emptyString = "";

    public EditTextListeners(IFragmentImplementation fragment) {
        this.fragment = fragment;
        attachTextWatcher();
        attachTouchListener();
    }

    public TextWatcher getTextWatcher() {
        return textWatcher;
    }

    private void setTextWatcher(TextWatcher textWatcher) {
        this.textWatcher = textWatcher;
    }

    public View.OnTouchListener getOnFocusChangeListener() {
        return onTouchListener;
    }

    private void setOnTouchListener(View.OnTouchListener onTouchListener) {
        this.onTouchListener = onTouchListener;
    }

    @SuppressLint("ClickableViewAccessibility")
    private void attachTouchListener() {
        setOnTouchListener((view, motionEvent) -> {
            if (view instanceof AppTextInputEditText) {
                ((AppTextInputEditText) view).setText(emptyString);
                view.requestFocus();
                fragment.stopPolling();
            }
            return false;
        });
    }

    private void attachTextWatcher() {
        setTextWatcher(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(fragment.isStoppedPolling()){
                    fragment.stopPolling();
                    fragment.attachInput(charSequence);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

}
