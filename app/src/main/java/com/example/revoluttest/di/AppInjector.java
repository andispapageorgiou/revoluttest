package com.example.revoluttest.di;

import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.revoluttest.base.BaseActivity;

public class AppInjector {
    private AppInjector() {

    }

    public static void inject(Activity activity) {
        ActivityInjector.get(activity).inject(activity);
    }

    public static void clearComponent(BaseActivity baseActivity) {
        ActivityInjector.get(baseActivity).clear(baseActivity);
    }

    public static void inject(@NonNull Fragment fragment) {
        if (fragment.getActivity() != null) {
            FragmentInjector.get(fragment.getActivity()).inject(fragment);
        }
    }
}
