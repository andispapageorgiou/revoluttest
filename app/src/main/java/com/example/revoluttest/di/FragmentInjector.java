package com.example.revoluttest.di;
import android.content.Context;

import androidx.fragment.app.Fragment;
import com.example.revoluttest.base.BaseFragment;
import com.example.revoluttest.base.RevolutApplication;
import com.example.revoluttest.ui.main.fragments.AddRatesFragment;
import com.example.revoluttest.ui.main.fragments.ConverterFragment;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Provider;

import dagger.android.AndroidInjector;

public class FragmentInjector {
    private final Map<Class<? extends Fragment>, Provider<AndroidInjector.Factory<? extends Fragment>>> fragmentInjectors;
    private final Map<String, AndroidInjector<? extends Fragment>> cache = new HashMap<>();

    @Inject
    FragmentInjector(Map<Class<? extends Fragment>, Provider<AndroidInjector.Factory<? extends Fragment>>> fragmentInjectors) {
        this.fragmentInjectors = fragmentInjectors;
    }

    void inject(Fragment fragment) {
        if (!(fragment instanceof BaseFragment)) {
            throw new IllegalArgumentException("Activity is not instance of BaseActivity");
        }
        String instanceId = "";
        if(fragment instanceof AddRatesFragment){
            instanceId = "0x0A";
        }
        else if(fragment instanceof ConverterFragment){
            instanceId = "0x0B";
        }
        if (cache.containsKey(instanceId)) {
            //noinspection unchecked
            AndroidInjector<Fragment> state = ((AndroidInjector<Fragment>) cache.get(instanceId));
            if (state != null)
                state.inject(fragment);
            return;
        }
        //noinspection unchecked/
        AndroidInjector.Factory<Fragment> injectorFactory = (AndroidInjector.Factory<Fragment>) fragmentInjectors.get(fragment.getClass()).get();
        AndroidInjector<Fragment> injector = injectorFactory.create(fragment);
        cache.put(instanceId, injector);
        injector.inject(fragment);
    }

    static FragmentInjector get(Context context) {
        return ((RevolutApplication) context.getApplicationContext()).getFragmentInjector();
    }
}
