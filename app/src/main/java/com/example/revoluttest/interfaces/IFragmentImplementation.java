package com.example.revoluttest.interfaces;

public interface IFragmentImplementation {
    void revokeAdapter();

    boolean stopPolling();

    boolean startPolling();

    boolean isStoppedPolling();

    void attachInput(CharSequence charSequence);
}
