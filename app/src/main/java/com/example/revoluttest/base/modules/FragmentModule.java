package com.example.revoluttest.base.modules;

import androidx.fragment.app.Fragment;

import com.example.revoluttest.MainActivity;
import com.example.revoluttest.ui.main.fragments.AddRatesFragment;
import com.example.revoluttest.ui.main.fragments.ConverterFragment;

import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.android.support.FragmentKey;
import dagger.multibindings.IntoMap;
@Module(subcomponents = {
        FragmentModule.AddRatesFragmentComponent.class,
        FragmentModule.ConverterFragmentComponent.class
})
public abstract class FragmentModule {

    @Binds
    @IntoMap
    @FragmentKey(AddRatesFragment.class)
    abstract AndroidInjector.Factory<? extends Fragment> provideRatesFragmentComponent(AddRatesFragmentComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(ConverterFragment.class)
    abstract AndroidInjector.Factory<? extends Fragment> provideConverterFragmentComponent(ConverterFragmentComponent.Builder builder);

    @Subcomponent
    public interface AddRatesFragmentComponent extends AndroidInjector<AddRatesFragment> {

        @Subcomponent.Builder
        abstract class Builder extends AndroidInjector.Builder<AddRatesFragment> {}
    }

    @Subcomponent
    public interface ConverterFragmentComponent extends AndroidInjector<ConverterFragment> {

        @Subcomponent.Builder
        abstract class Builder extends AndroidInjector.Builder<ConverterFragment> {}
    }
}
