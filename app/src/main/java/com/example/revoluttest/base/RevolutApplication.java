package com.example.revoluttest.base;

import android.app.Activity;
import android.app.Application;

import com.example.revoluttest.base.modules.ApplicationModule;
import com.example.revoluttest.di.ActivityInjector;
import com.example.revoluttest.di.FragmentInjector;

import javax.inject.Inject;

public class RevolutApplication extends Application {

    @Inject
    ActivityInjector activityInjector;

    @Inject
    FragmentInjector fragmentInjector;

    @Override
    public void onCreate() {
        super.onCreate();

        ApplicationComponent component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        component.inject(this);
    }

    public static RevolutApplication get(Activity activity){
        return (RevolutApplication) activity.getApplication();
    }
    public ActivityInjector getActivityInjector() {
        return activityInjector;
    }

    public FragmentInjector getFragmentInjector() {
        return fragmentInjector;
    }
}
