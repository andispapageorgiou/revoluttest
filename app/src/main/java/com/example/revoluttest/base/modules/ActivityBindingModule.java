package com.example.revoluttest.base.modules;

import android.app.Activity;

import com.example.revoluttest.MainActivity;
import com.example.revoluttest.di.ActivityScope;

import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.ActivityKey;
import dagger.android.AndroidInjector;
import dagger.multibindings.IntoMap;

@Module(subcomponents = {
        ActivityBindingModule.MainActivityComponent.class,
})
public abstract class ActivityBindingModule {

    @Binds
    @IntoMap
    @ActivityKey(MainActivity.class)
    abstract AndroidInjector.Factory<? extends Activity> provideMainActivityInjector(MainActivityComponent.Builder builder);

    @Subcomponent
    public interface MainActivityComponent extends AndroidInjector<MainActivity> {

        @Subcomponent.Builder
        abstract class Builder extends AndroidInjector.Builder<MainActivity> {
        }
    }
}
