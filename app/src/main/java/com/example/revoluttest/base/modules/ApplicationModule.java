package com.example.revoluttest.base.modules;

import android.app.Application;
import android.content.Context;

import com.example.revoluttest.di.ActivityScope;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {
    private final Application application;

    public ApplicationModule(Application application){
        this.application = application;
    }

    @Provides
    @ActivityScope
    Context provideApplicationContext(){
        return application;
    }
}
