package com.example.revoluttest.base.modules;

import com.example.revoluttest.services.Service;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public abstract class RetrofitServiceModule {

    @Provides
    @Singleton
    static Service provideService(Retrofit retrofit) {
        return retrofit.create(Service.class);
    }
}
