package com.example.revoluttest.base;

import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DiffUtil;

import com.example.revoluttest.adapters.RatesRecyclerAdapter;
import com.example.revoluttest.di.AppInjector;
import com.example.revoluttest.services.RatesRequester;
import com.example.revoluttest.ui.main.viewmodel.RatesVM;

import javax.inject.Inject;

public abstract class BaseFragment extends Fragment {

    @Inject
    RatesRequester ratesRequester;
    protected RatesVM ratesVM;
    protected ViewDataBinding dataBinding;
    protected RatesRecyclerAdapter ratesRecyclerAdapter;
    protected RatesRecyclerAdapter ratesRecyclerAdapterRates;
    protected DiffUtil.DiffResult diffResult;

    public abstract ViewDataBinding dataBinding();


    @LayoutRes
    public abstract int layoutRes();

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ratesVM = ViewModelProviders.of(this).get(RatesVM.class);
        ratesVM.setPageRequester(ratesRequester);
        ratesVM.setContext(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        AppInjector.inject(this);
        dataBinding = dataBinding();
        dataBinding = DataBindingUtil.inflate(inflater, layoutRes(), container, false);
        dataBinding.setLifecycleOwner(this);
        return dataBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setObservers();
    }

    private void setObservers() {
        ratesVM.ratesLiveData.observe(this, ratesList -> {
            if (ratesList != null && ratesList.size() > 0) {
                if (ratesRecyclerAdapter != null)
                    ratesRecyclerAdapter.getResults().accept(new Pair<>(diffResult, ratesList));

                if (ratesRecyclerAdapterRates != null) {
                    ratesRecyclerAdapterRates.getResults().accept(new Pair<>(diffResult, ratesList));
                }
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        if (ratesRecyclerAdapter != null) {
            ratesRecyclerAdapter.onDetach();
        }

        if (ratesRecyclerAdapterRates != null) {
            ratesRecyclerAdapterRates.onDetach();
        }
        super.onDestroy();
    }

}
