package com.example.revoluttest.base;
import com.example.revoluttest.base.modules.ActivityBindingModule;
import com.example.revoluttest.base.modules.ApplicationModule;
import com.example.revoluttest.base.modules.FragmentModule;
import com.example.revoluttest.base.modules.RetrofitServiceModule;
import com.example.revoluttest.base.modules.ServiceModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        ApplicationModule.class,
        ActivityBindingModule.class,
        FragmentModule.class,
        ServiceModule.class,
        RetrofitServiceModule.class,
})
public interface ApplicationComponent {
    void inject(RevolutApplication application);

}
