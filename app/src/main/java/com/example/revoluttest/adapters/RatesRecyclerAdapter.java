package com.example.revoluttest.adapters;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.text.TextWatcher;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.revoluttest.R;
import com.example.revoluttest.interfaces.IFragmentImplementation;
import com.example.revoluttest.models.RatesAdapterItem;
import com.google.android.material.textfield.TextInputEditText;
import com.jakewharton.rxrelay2.BehaviorRelay;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class RatesRecyclerAdapter extends RecyclerView.Adapter<RatesRecyclerAdapter.ViewHolder> {
    public static final int RATES_VIEW = 1;
    public static final int CONVERTER_VIEW = 2;
    private List<RatesAdapterItem> ratesArrayList;
    private BehaviorRelay<Pair<DiffUtil.DiffResult, List<RatesAdapterItem>>>
            listBehaviorRelay = BehaviorRelay.create();

    private int viewType;
    private ViewHolder viewHolder;
    private BehaviorRelay<Boolean> processing = BehaviorRelay.create();
    //Todo on DESTROY
    private Disposable disposable;

    public BehaviorRelay<Pair<DiffUtil.DiffResult, List<RatesAdapterItem>>> getResults() {
        return listBehaviorRelay;
    }

    private IFragmentImplementation fragment;

    public RatesRecyclerAdapter(IFragmentImplementation fragment, int viewType,
                                List<RatesAdapterItem> ratesArrayList) {
        this.viewType = viewType;
        this.ratesArrayList = ratesArrayList;
        this.fragment = fragment;
    }

    @Override
    public int getItemViewType(int position) {
        return viewType;
    }

    @Override
    public long getItemId(int position) {
        return this.ratesArrayList.get(position).getId();
    }

    @Override
    public int getItemCount() {
        return this.ratesArrayList.size();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        //RATES VIEW
        View viewRoot;
        if (viewType == RATES_VIEW) {
            viewRoot = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_rates_adapter_item, parent, false);
            viewHolder = new ViewHolder(viewRoot, RATES_VIEW);
            return viewHolder;

            //CONVERTER VIEW
        } else if (viewType == CONVERTER_VIEW) {
            viewRoot = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_converter_adapter_item, parent, false);
            viewHolder = new ViewHolder(viewRoot, CONVERTER_VIEW);
            return viewHolder;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        RatesAdapterItem ratesAdapterItem = this.ratesArrayList.get(position);
        loadImage(holder.flagImageView, ratesAdapterItem.flag);//Glide
        holder.codeTextView.setText(ratesAdapterItem.getCode());
        holder.nameTextView.setText(ratesAdapterItem.getName());
        String value = ratesAdapterItem.getValue() != null ? String.valueOf(ratesAdapterItem.getValue()) : "";

        if (holder.valueText != null)
            holder.valueText.setText(value);

        if (holder.valueEditText != null)
            holder.valueEditText.setText(value);

    }

    private void loadImage(@NonNull ImageView view, @NonNull Drawable drawable) {
        try {
            Glide.with(view.getContext())
                    .load(drawable).apply(new RequestOptions().circleCrop())
                    .dontAnimate()
                    .into(view);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Currency Converter
     *
     * @param selectedCurrency
     * @param input
     */
    public void currencyConverterFilter(RatesAdapterItem selectedCurrency, double input) {
        List<RatesAdapterItem> cacheList = new ArrayList<>(this.ratesArrayList);
        cacheList.forEach(currency -> {
            try {
                if (selectedCurrency != currency && input != 0) {
                    double convertedRate = input * currency.getValue();
                    int positionItem = cacheList.indexOf(currency);
                    if (positionItem == -1 || positionItem == 0) return;
                    ratesArrayList.get(positionItem).setValue(convertedRate);
                    notifyItemChanged(positionItem);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public void subscribeToRatesUpdate() {
        disposable = getResults()
                .doOnComplete(() -> processing.accept(true))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(seedPair -> {
                    DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(
                            new DiffCallBackRates(this.ratesArrayList, seedPair.second));
                    if (!fragment.isStoppedPolling()) {
                        this.ratesArrayList.clear();
                        this.ratesArrayList.addAll((seedPair.second));
                    }
                    return diffResult;
                }).subscribe(this::accept, Throwable::printStackTrace);
    }

    private void accept(DiffUtil.DiffResult diffResult) {
        if (fragment.isStoppedPolling()) return;
        diffResult.dispatchUpdatesTo(this);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        int viewType;
        TextView valueText;
        TextInputEditText valueEditText;
        ImageView flagImageView;
        TextView codeTextView;
        TextView nameTextView;

        ViewHolder(View view, int viewTypeCase) {
            super(view);
            viewType = viewTypeCase;
            if (viewTypeCase == CONVERTER_VIEW)
                valueEditText = view.findViewById(R.id.value_edit_text);

            else if (viewTypeCase == RATES_VIEW)
                valueText = view.findViewById(R.id.value_text);

            flagImageView = view.findViewById(R.id.flag);
            codeTextView = view.findViewById(R.id.code);
            nameTextView = view.findViewById(R.id.name);
        }
    }

    public void onDetach() {
        if (disposable.isDisposed()) {
            disposable.dispose();
        }
    }
}
