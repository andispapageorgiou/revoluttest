package com.example.revoluttest.adapters;

import android.text.TextUtils;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;

import com.example.revoluttest.models.RatesAdapterItem;

import java.util.List;

public class DiffCallBackRates extends DiffUtil.Callback {

    private final List<RatesAdapterItem> oldRatesList;
    private final List<RatesAdapterItem> newRatesList;

    DiffCallBackRates(List<RatesAdapterItem> oldRatesList, List<RatesAdapterItem> newRatesList) {
        this.oldRatesList = oldRatesList;
        this.newRatesList = newRatesList;
    }

    @Override
    public int getOldListSize() {

        return oldRatesList.size();
    }

    @Override
    public int getNewListSize() {
        return newRatesList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldRatesList.get(oldItemPosition).getId() == newRatesList.get(newItemPosition).getId();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return oldRatesList.get(oldItemPosition).getFlag().equals(newRatesList.get(newItemPosition).getFlag())
                && oldRatesList.get(oldItemPosition).getValue().equals(newRatesList.get(newItemPosition).getValue());
    }

    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        return super.getChangePayload(oldItemPosition, newItemPosition);
    }
}