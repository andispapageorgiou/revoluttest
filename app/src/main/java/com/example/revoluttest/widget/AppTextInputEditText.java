package com.example.revoluttest.widget;

import android.content.Context;
import android.util.AttributeSet;

import com.google.android.material.textfield.TextInputEditText;

public class AppTextInputEditText extends TextInputEditText {
    public AppTextInputEditText(Context context) {
        super(context);
    }
    public AppTextInputEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean isClickable() {
        return true;
    }

    @Override
    public CharSequence getHint() {
        return super.getHint();
    }

    @Override
    public boolean performClick() {
        super.performClick();
        return true;
    }
}
