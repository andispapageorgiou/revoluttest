package com.example.revoluttest.models;

import com.google.auto.value.AutoValue;
import com.squareup.moshi.Json;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

@AutoValue
public abstract class CurrencyResponse {

    @Json(name = "base")
    public abstract String base();

    @Json(name = "date")
    public abstract String date();

    @Json(name = "rates")
    public abstract Object rates();

    public static JsonAdapter<CurrencyResponse> jsonAdapter(Moshi moshi) {
        return new AutoValue_CurrencyResponse.MoshiJsonAdapter(moshi);
    }
}
