package com.example.revoluttest.models;
import android.graphics.drawable.Drawable;
import com.example.revoluttest.interfaces.AdapterObject;

public class RatesAdapterItem extends AdapterObject {
    private Double value;
    private String code;
    public String name;
    public Drawable flag;
    public int id;

    public RatesAdapterItem(int id, String key, Double value) {
        this.code = key;
        this.value = value;
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public Drawable getFlag() {
        return flag;
    }

    public Double getValue() { return value; }

    public int getId() {
        return id;
    }

    public void setValue(Double value) {
        this.value = value;
    }
}
